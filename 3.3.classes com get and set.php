<!DOCTYPE HTML>
<html>
  <head>
    <title>PHP</title>
  </head>
  <body>
    <?php
	
		class Soma {
			
			private $x, $y;
			
			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x + $this->y;
			}
			
			public function getX(){
				return $this->x;
			}
			
			public function setX($x){
				$this->x = $x;
			}
			
			public function getY(){
				return $this->y;
			}
			
			public function setY($y){
				$this->y = $y;
			}
			
		}
		
		class Subtracao {
			
			public $x, $y;

			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x - $this->y;
			}
			
		}
		
		$soma = new Soma(5,2);
		$subtracao = new Subtracao(5,2);
		
		$soma->setX(3);
		$subtracao->x = 4;
		//$soma->x = 4; d� o erro "cannot access private property"
		
		echo $soma->calcular() . "<br>";
		echo $subtracao->calcular();
		
    ?>
  </body>
</html>