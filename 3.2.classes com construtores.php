<!DOCTYPE HTML>
<html>
  <head>
    <title>PHP</title>
  </head>
  <body>
    <?php
	
		class Soma {
			
			public $x, $y;
			
			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x + $this->y;
			}
			
		}
		
		class Subtracao {
			
			public $x, $y;

			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x - $this->y;
			}
			
		}
		
		$soma = new Soma(5,2);
		
		$subtracao = new Subtracao(5,2);
		
		//tentar isso
		//$soma->x = 3;
		//$subtracao->y = 1;
		
		echo $soma->calcular() . "<br>";
		echo $subtracao->calcular();
		
    ?>
  </body>
</html>