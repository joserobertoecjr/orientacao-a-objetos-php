<!DOCTYPE HTML>
<html>
  <head>
    <title>PHP</title>
  </head>
  <body>
    <?php
	
		class Soma {
			
			public function calcular($x, $y) {
				return $x + $y;
			}
			
		}
		
		class Subtracao {
			
			public function calcular($x, $y) {
				return $x - $y;
			}
			
		}
		
		$soma = new Soma();
		
		$subtracao = new Subtracao();
		
		echo $soma->calcular(5,2) . "<br>";
		echo $subtracao->calcular(5,2);
		
    ?>
  </body>
</html>