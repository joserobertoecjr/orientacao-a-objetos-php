<!DOCTYPE HTML>
<html>
  <head>
    <title>PHP</title>
  </head>
  <body>
    <?php
	
		class Operacao {

			public function calcular(){
				
			}
			
			public function imprimir(){
				return "O calculo teve o valor de {calcular()} <br>";
			}
		}
	
		class Soma extends Operacao{
			
			private $x, $y;
			
			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x + $this->y;
			}
			
			public function getX(){
				return $this->x;
			}
			
			public function setX($x){
				$this->x = $x;
			}
			
			public function getY(){
				return $this->y;
			}
			
			public function setY($y){
				$this->y = $y;
			}
			
		}
		
		class Subtracao extends Operacao{
			
			public $x, $y;

			function __construct($x, $y){
				$this->x = $x;
				$this->y = $y;
			}
			
			public function calcular() {
				return $this->x - $this->y;
			}
			
			public function imprimir(){
				return "A subtracao teve o valor de {calcular()} <br>";
			}
			
		}
		
		$soma = new Soma(5,2);
		$subtracao = new Subtracao(5,2);
		
		$soma->setX(3);
		
		echo $soma->imprimir() . "<br>";
		echo $subtracao->imprimir();
		
    ?>
  </body>
</html>